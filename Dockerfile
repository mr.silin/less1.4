FROM debian:9 as build
RUN apt update && apt install -y make wget gcc libpcre++-dev libssl-dev libxslt1-dev build-essential zlib1g-dev openssl
RUN wget http://nginx.org/download/nginx-1.19.6.tar.gz && \ 
    tar xvfz nginx-1.19.6.tar.gz && \
    cd nginx-1.19.6 && \
    ./configure \
    #--with-ld-opt="-Wl,-rpath,/path/to/luajit/lib" --add-module=/path/to/ngx_devel_kit --add-module=/path/to/lua-nginx-module 
    && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
